'use strict';

(function() {

function OrderResource($resource, appConfig) {
	return $resource(appConfig.envURL + '/api/orders/:id/:controller', {
		id: '@_id'
	}, {
		update: {
			method: 'PUT'
		},
		byUser: {
			method: 'GET',
			isArray: true,
			params: {
				controller:'user'
			}
		}
	});
}

angular.module('toDomicilio')
  .factory('Order', OrderResource);

})();
