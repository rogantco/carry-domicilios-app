'use strict';

angular.module('toDomicilio')
  .controller('RestaurantDirCtrl', ['appConfig', '$scope', function(appConfig, $scope) {
  	var vm = this;

  	vm.url = appConfig.envURL;
  	var weekDays = ['sunday', 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday'];
  	var today = weekDays[new Date().getDay()];

  	$scope.restaurant.isOpen = isOpen($scope.restaurant.workingTime[today]);

  	function isOpen(workingTime){
  		var now = new Date();
  		var open = new Date(new Date().setHours(new Date(workingTime.open).getHours(), new Date(workingTime.open).getMinutes()))
  		var closed = new Date(new Date().setHours(new Date(workingTime.closed).getHours(), new Date(workingTime.closed).getMinutes()))

  		if(open < now && closed > now){
  			return true
  		}else{
  			return false
  		}
  	}
  }])
  .directive('restaurant', function(){
    return {
      templateUrl: 'components/restaurant/restaurant.html',
      restrict: 'E',
      controller: 'RestaurantDirCtrl as vm',
      scope:true      
    }
  });
