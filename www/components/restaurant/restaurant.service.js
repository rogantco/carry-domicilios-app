'use strict';

(function() {

function RestaurantResource($resource, appConfig) {
	return $resource(appConfig.envURL + '/api/restaurants/:id/:controller', {
		id: '@_id'
	});
}

angular.module('toDomicilio')
  .factory('Restaurant', RestaurantResource);

})();
