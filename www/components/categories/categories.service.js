'use strict';

(function() {

function CategoriesResource($resource, appConfig) {
  return $resource(appConfig.envURL + '/api/categories/:id/:controller', {
		id: '@_id'
	}, {
		update: {
			method: 'PUT'
		}
	});
}

angular.module('toDomicilio')
  .factory('Categories', CategoriesResource);

})();
