'use strict';

(function() {

function UserResource($resource, appConfig) {
  return $resource(appConfig.envURL + '/api/users/:id/:controller', {
    id: '@_id'
  }, {
    changePassword: {
      method: 'PUT',
      params: {
        controller:'password'
      }
    },
    get: {
      method: 'GET',
      params: {
        id:'me'
      }
    },
    update: {
      method: 'PUT'
    }   
  });
}

angular.module('toDomicilio.auth')
  .factory('User', UserResource);

})();
