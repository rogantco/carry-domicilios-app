'use strict';

angular.module('toDomicilio.auth', [
  'toDomicilio.constants',
  'toDomicilio.util'
])
  .config(function($httpProvider) {
    $httpProvider.interceptors.push('authInterceptor');
  });
