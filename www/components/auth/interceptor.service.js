'use strict';

(function() {

function authInterceptor($rootScope, $q, $location) {
  var storage = window.localStorage;

  return {
    // Add authorization token to headers
    request: function(config) {
      config.headers = config.headers || {};
      if (storage.getItem('token')) {
        config.headers.Authorization = 'Bearer ' + storage.getItem('token');
      }
      return config;
    },

    // Intercept 401s and redirect you to login
    responseError: function(response) {
      if (response.status === 401) {
        $location.path('/app/login');
        // remove any stale tokens
        storage.removeItem('token');
      }
      return $q.reject(response);
    }
  };
}

angular.module('toDomicilio.auth')
  .factory('authInterceptor', authInterceptor);

})();
