'use strict';

function NavbarController(Auth, Restaurant) {
  var vm = this;

  vm.isLoggedIn = Auth.isLoggedIn;
  vm.isAdmin = Auth.isAdmin;

  Restaurant.query(function(restaurants){
  	angular.forEach(restaurants, function(restaurant){
  		if(restaurant.name === 'Víveres'){
  			vm.superMarket = restaurant._id;
  		}
  	});
  })
}

angular.module('toDomicilio')
  .controller('NavbarController', NavbarController);
