'use strict';

angular.module('toDomicilio')
	.controller('RestaurantCtrl', function ($scope, $stateParams, Restaurant, appConfig, Auth, $location, $ionicModal, $ionicPopup, Order, $cordovaGoogleAnalytics) {
		var vm = this;

		vm.url = appConfig.envURL;
		vm.orderHelper = {}
		vm.restaurant = Restaurant.get({id: $stateParams.id}, function(){
			vm.orderHelper.order = {
				restaurant: vm.restaurant._id,
				dishes: [],
				paymentMethod: 'Efectivo',
				deliveryCharge: vm.restaurant.deliveryCharge,
				total: vm.restaurant.deliveryCharge
			}
		});

		vm.toggleGroup = function(group) {
			if (vm.isGroupShown(group)) {
				vm.shownGroup = null;
			} else {
				vm.shownGroup = group;
			}
		};

		vm.isGroupShown = function(group) {
			return vm.shownGroup === group;
		};

		vm.orderHelper.addToCar = function(menuKey, dishKey){
			if(Auth.isLoggedIn()){
				vm.user = Auth.getCurrentUser();
				vm.orderHelper.order.customer = vm.user._id;
				vm.orderHelper.order.dishes.push({
					name: vm.restaurant.menu[menuKey].dishes[dishKey].name,
					description: vm.restaurant.menu[menuKey].dishes[dishKey].description,
					price: vm.restaurant.menu[menuKey].dishes[dishKey].price
				});
				vm.orderHelper.order.total += vm.restaurant.menu[menuKey].dishes[dishKey].price;
				vm.orderHelper.order.subTotal = vm.orderHelper.order.total - vm.orderHelper.order.deliveryCharge;
				vm.orderHelper.order.deliveryAddress = vm.user.addresses[0];
			}else{
				$location.path('/app/login');
			}
		}

		vm.orderHelper.removeToCar = function(key){
			vm.orderHelper.order.total -= vm.orderHelper.order.dishes[key].price;
			vm.orderHelper.order.subTotal = vm.orderHelper.order.total - vm.orderHelper.order.deliveryCharge;
			vm.orderHelper.order.dishes.splice(key, 1);
		}

		$ionicModal.fromTemplateUrl('app/restaurant/order.modal.html', {
			scope: $scope,
			animation: 'slide-in-up'
		}).then(function(modal) {
			vm.ordenModal = modal;
		});

		vm.orderHelper.fireOrdenModal = function(){
			vm.ordenModal.show();

			if(window.cordova){
				$cordovaGoogleAnalytics.trackView('app.restaurant' + '.' + vm.restaurant._id + '.order');
			}
		}

		vm.orderHelper.closeOrderModal = function(){
			vm.ordenModal.hide();
		}

		$ionicModal.fromTemplateUrl('app/restaurant/address.modal.html', {
			scope: $scope,
			animation: 'slide-in-down'
		}).then(function(modal) {
			vm.addressModal = modal;
		});

		vm.orderHelper.orderModalToAddress = function(){
			if(vm.orderHelper.order.subTotal >= vm.restaurant.minimumOrder){
				vm.orderHelper.closeOrderModal();
				vm.addressModal.show();

				if(window.cordova){
					$cordovaGoogleAnalytics.trackView('app.restaurant' + '.' + vm.restaurant._id + '.address');
				}
			}else{
				var alertPopup = $ionicPopup.alert({
					title: 'Pedido minimo',
					template: 'Tu pedido es menor que la orden minima'
				});

				if(window.cordova){
					$cordovaGoogleAnalytics.trackView('app.restaurant' + '.' + vm.restaurant._id + '.order.minor');
				}

				alertPopup.then(function(res) {
					vm.orderHelper.closeOrderModal();
				});
			}
		}

		vm.orderHelper.addressPicker = function(){
			var myPopup = $ionicPopup.confirm({
				templateUrl: 'app/restaurant/addresspicker.popup.html',
				title: 'Mis Direcciones',
				scope: $scope
			});

			myPopup.then(function(res) {
				if(res && vm.orderHelper.selectedAddres){
					vm.orderHelper.order.deliveryAddress = vm.user.addresses[vm.orderHelper.selectedAddres];
				}
			});

			if(window.cordova){
				$cordovaGoogleAnalytics.trackView('app.restaurant' + '.' + vm.restaurant._id + '.address.picker');
			}
		}

		vm.orderHelper.sendOrder = function(){
			vm.order = new Order(vm.orderHelper.order);
			vm.order.$save().then(function(order) {
				var alertPopup = $ionicPopup.alert({
					title: 'Pedido',
					template: 'Tu pedido fue enviado exitosamente, pronto recibiras la confirmación.'
				});
				if(window.cordova){
					$cordovaGoogleAnalytics.trackView('app.restaurant' + '.' + vm.restaurant._id + '.order.confirm');
					$cordovaGoogleAnalytics.addTransaction(vm.order._id, vm.restaurant.name, vm.order.subTotal, 0, vm.order.deliveryCharge, 'COP');

					angular.forEach(vm.order.dishes, function(dish) {
						$cordovaGoogleAnalytics.addTransactionItem(vm.order._id, dish.name, dish._id, vm.order.restaurant, dish.price, 1, 'COP');
					});
				}

				alertPopup.then(function(res) {
					vm.addressModal.hide();
					$location.path('/app/home');
				});
			}).catch(function(err){
				var alertPopup = $ionicPopup.alert({
					title: 'Error',
					template: 'Tu pedido no fue enviado por favor intentalo de nuevo'
				});

				if(window.cordova){
					$cordovaGoogleAnalytics.trackView('app.restaurant' + '.' + vm.restaurant._id + '.order.fail');
				}
			});
		}
	});