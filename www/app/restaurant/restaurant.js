'use strict';

angular.module('toDomicilio')
	.config(function ($stateProvider) {
		$stateProvider.state('app.restaurant', {
			url: '/restaurant/:id',
			views: {
				'menuContent': {
					templateUrl: 'app/restaurant/restaurant.html',
					controller: 'RestaurantCtrl as vm'
				}
			}
		})
	});