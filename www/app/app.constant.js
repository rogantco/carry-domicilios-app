(function(angular, undefined) {
'use strict';

angular.module('toDomicilio.constants', [])

.constant('appConfig', {userRoles:['guest','user','admin'],streetTypes:['Calle','Carrera','Avenida','Avenida Carrera','Avenida Calle','Circular','Circunvalar','Diagonal','Manzana','Transversal','Vía'], envURL: 'http://todomicilio.com.co'})

;
})(angular);