'use strict';

angular.module('toDomicilio')
  .config(function ($stateProvider) {
	  $stateProvider.state('app.home', {
	    url: '/home',
		views: {
			'menuContent': {
				templateUrl: 'app/home/home.html',
				controller: 'HomeCtrl as vm'
			}
		}
	  })
  });
