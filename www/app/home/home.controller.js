'use strict';

angular.module('toDomicilio')
	.controller('HomeCtrl', function (Restaurant, Categories, $scope) {
		var vm = this;

		vm.categories = Categories.query();
		vm.restaurants = Restaurant.query();
		vm.currentCategory = '';

		vm.refreshRestaurants = function(){
			vm.restaurants = Restaurant.query();
			$scope.$broadcast('scroll.refreshComplete');
		}
	});