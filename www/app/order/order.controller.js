'use strict';

angular.module('toDomicilio')
	.controller('OrderCtrl', function (Order, $stateParams) {
		var vm = this;
		vm.order = Order.get({id: $stateParams.id});
	});