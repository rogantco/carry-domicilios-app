'use strict';

angular.module('toDomicilio')
  .config(function ($stateProvider) {
	  $stateProvider.state('app.order', {
	    url: '/order/:id',
		views: {
			'menuContent': {
				templateUrl: 'app/order/order.html',
				controller: 'OrderCtrl as vm'
			}
		}
	  })
  });
