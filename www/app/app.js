// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'toDomicilio' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
angular.module('toDomicilio', [
  'ionic',
  'toDomicilio.auth',
  'toDomicilio.constants',
  'ngResource',
  'ngSanitize',
  'ng-currency',
  'validation.match',
  'angularMoment',
  'ngCordova'
])

.run(function($ionicPlatform, amMoment, $cordovaGoogleAnalytics, $rootScope) {
  amMoment.changeLocale('es');

  $ionicPlatform.ready(function() {
    if(window.cordova){
      $cordovaGoogleAnalytics.startTrackerWithId('UA-75463806-2');
      $cordovaGoogleAnalytics.trackView('app.home');

      $rootScope.$on('$ionicView.enter', function( scopes, states) {
        if(states.stateParams && states.stateParams.id){
          $cordovaGoogleAnalytics.trackView(states.stateName + '.' + states.stateParams.id);
        }else{
          $cordovaGoogleAnalytics.trackView(states.stateName);
        }
      });

      if(window.cordova.plugins.Keyboard) {
        // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
        // for form inputs)
        cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);

        // Don't remove this line unless you know what you are doing. It stops the viewport
        // from snapping when text inputs are focused. Ionic handles this internally for
        // a much nicer keyboard experience.
        cordova.plugins.Keyboard.disableScroll(true);
      }
      if(window.StatusBar) {
        StatusBar.styleDefault();
      }
    }
  });
})

.config(function($stateProvider, $urlRouterProvider, $ionicConfigProvider) {
  $ionicConfigProvider.tabs.position('bottom');
  $ionicConfigProvider.tabs.style('standard');
  $stateProvider.state('app', {
    url: '/app',
    abstract: true,
    templateUrl: 'components/navbar/navbar.html',
    controller: 'NavbarController as vm'
  })

  $urlRouterProvider.otherwise('/app/home');
});
