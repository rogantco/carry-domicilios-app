'use strict';

function SettingsController(Auth, appConfig, $ionicPopup, $location, $timeout, Order, $scope, $cordovaGoogleAnalytics) {
  var vm = this;
  //start-non-standard
  vm.errors = {};
  vm.submitted = false;
  //end-non-standard

  vm.Auth = Auth;
  vm.streetTypes = appConfig.streetTypes;
  vm.updatePassword = {};
  vm.Auth.getCurrentUser().$promise.then(function(user) {
    vm.user = user;

    if(vm.user.addresses.length === 0){
      vm.addAddress();
    }

    vm.orders = Order.byUser({id: vm.user._id});
  });


  vm.addAddress = function(){
    vm.user.addresses.push({
      description: '',
      streetType: '',
      num1: '',
      num2: '',
      num3: '',
      address2: '',
      phone: '',
      featured: false
    });

    vm.submitted = false;
  };

  vm.removeAddress = function(key){
    var confirmPopup = $ionicPopup.confirm({
      title: 'Eliminar Dirección',
      template: 'Está seguro que desea eliminar la dirección?'
    });

    confirmPopup.then(function(res) {
      if(res) {
        vm.user.addresses.splice(key, 1);
        vm.shouldShowDelete = false;

        vm.user.$update()
          .then(function(){
            if(vm.user.addresses.length === 0){
              vm.addAddress();
            }

            vm.submitted = false;
          })
          .catch(function(err) {
            err = err.data;
            vm.errors = {};

            // Update validity of form fields that match the mongoose errors
            angular.forEach(err.errors, function(error, field) {
              form[field].$setValidity('mongoose', false);
              vm.errors[field] = error.message;
            });
          });
      }else{
        vm.shouldShowDelete = false;
      }
    });
  };

  vm.updateUser = function(){
    vm.submittedUpdate = true;

    if (vm.addressForm.$valid) {    
      vm.user.$update()
        .then(function() {
          var alertPopup = $ionicPopup.alert({
            title: 'Dirección actualizada',
            template: 'Dirección actualizada exitosamente'
          });

          alertPopup.then(function(res) {
            vm.submittedUpdate = false;
            $location.path('/');
          });
        })
        .catch(function(err) {
          err = err.data;
          vm.errors = {};

          // Update validity of form fields that match the mongoose errors
          angular.forEach(err.errors, function(error, field) {
            vm.addressForm[field].$setValidity('mongoose', false);
            vm.errors[field] = error.message;
          });
        });
    }
  }

  vm.changePassword = function() {
    vm.submitted = true;

    if (vm.passwordForm.$valid) {
      vm.Auth.changePassword(vm.updatePassword.oldPassword, vm.updatePassword.newPassword)
        .then(function() {
          var alertPopup = $ionicPopup.alert({
            title: 'Contraseña actualizada',
            template: 'Contraseña actualizada exitosamente'
          });

          alertPopup.then(function(res) {
            $location.path('/');
            vm.updatePassword = {}
            vm.submitted = false;
          });
        })
        .catch(function() {
          vm.passwordForm.password.$setValidity('mongoose', false);
          vm.errors.other = 'Contraseña Incorrecta';
          vm.message = '';
        });
    }
  }

  vm.refreshOrders = function(){
    Order.byUser({id: vm.user._id}).$promise.then(function(orders) {
      vm.orders = orders;
      $scope.$broadcast('scroll.refreshComplete');
    });
  }

  vm.onTabSelected = function(section){
    if(window.cordova){
      $cordovaGoogleAnalytics.trackView('app.settings' + '.' + section);
    }
  }
}


angular.module('toDomicilio')
  .controller('SettingsController', SettingsController);
