'use strict';

function LoginController(Auth, $location) {
  var vm = this;
  //start-non-standard
  vm.user = {};
  vm.errors = {};
  vm.submitted = false;
  //end-non-standard

  vm.Auth = Auth;
  vm.$location = $location;

  vm.login = function(form) {
    vm.submitted = true;

    if (form.$valid) {
      vm.Auth.login({
        email: vm.user.email,
        password: vm.user.password
      })
      .then(function() {
        // Logged in, redirect to home
        vm.$location.path('/');
      })
      .catch(function(err) {
        vm.errors.other = err.message;
      });
    }
  }
}

angular.module('toDomicilio')
  .controller('LoginController', LoginController);
