'use strict';

function SignupController(Auth, $location, appConfig) {
  var vm = this;

  //start-non-standard
  vm.user = {};
  vm.user.addresses = [];
  vm.errors = {};
  vm.submitted = false;
  //end-non-standard

  vm.Auth = Auth;
  vm.$location = $location;
  vm.streetTypes = appConfig.streetTypes;

  vm.register = function(form) {
    vm.submitted = true;

    if (form.$valid) {
      vm.Auth.createUser(vm.user)
      .then(function() {
        // Account created, redirect to home
        vm.$location.path('/');
      })
      .catch(function(err) {
        err = err.data;
        vm.errors = {};

        // Update validity of form fields that match the mongoose errors
        angular.forEach(err.errors, function(error, field) {
          form[field].$setValidity('mongoose', false);
          vm.errors[field] = error.message;
        });
      });
    }
  }
}

angular.module('toDomicilio')
  .controller('SignupController', SignupController);
