'use strict';

angular.module('toDomicilio')
  .config(function($stateProvider) {
    $stateProvider.state('app.login', {
      url: '/login',
      views: {
        'menuContent': {
          templateUrl: 'app/account/login/login.html',
          controller: 'LoginController as vm'
        }
      }
    })

    $stateProvider.state('app.logout', {
      url: '/logout',
      views: {
        'menuContent': {
          name: 'logout',
          referrer: '/',
          template: '',
          controller: function($location, Auth) {
            Auth.logout();
            $location.path('/');
          }
        }
      }
    }) 

    $stateProvider.state('app.signup', {
      url: '/signup',
      views: {
        'menuContent': {
          templateUrl: 'app/account/signup/signup.html',
          controller: 'SignupController',
          controllerAs: 'vm'
        }
      }
    })

    $stateProvider.state('app.settings', {
      url: '/settings',
      views: {
        'menuContent': {
          templateUrl: 'app/account/settings/settings.html',
          controller: 'SettingsController',
          controllerAs: 'vm',
          authenticate: true
        }
      }
    })
  }) 
  .run(function($rootScope) {
    $rootScope.$on('$routeChangeStart', function(event, next, current) {
      if (next.name === 'logout' && current && current.originalPath && !current.authenticate) {
        next.referrer = current.originalPath;
      }
    });
  });
