cordova build --release android

jarsigner -verbose -sigalg SHA1withRSA -digestalg SHA1 -keystore realise/releaseKey.keystore platforms/android/build/outputs/apk/android-release-unsigned.apk toDomicilio

rm realise/toDomicilio.apk; zipalign -v 4 platforms/android/build/outputs/apk/android-release-unsigned.apk realise/toDomicilio.apk